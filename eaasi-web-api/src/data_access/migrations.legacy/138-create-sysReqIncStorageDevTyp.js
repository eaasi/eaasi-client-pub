'use strict';

const Sq = require('sequelize');

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('system_requirements_requires_storage_device', {
			createdAt: Sq.DATE,
			updatedAt: Sq.DATE,
			systemRequirementsID: {
				type: Sq.INTEGER,
				allowNull: false,
				references: {
					model: 'system_requirements',
					key: 'id'
				}
			},
			storageDeviceTypeID: {
				type: Sq.INTEGER,
				allowNull: false,
				references: {
					model: 'storage_device_type',
					key: 'id'
				}
			}
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable('system_requirements_requires_storage_device_type');
		return queryInterface.dropTable('system_requirements_requires_storage_device');
	}
};
