'use strict';

const Sq = require('sequelize');

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('os_version', {
			createdAt: Sq.DATE,
			updatedAt: Sq.DATE,
			id: {
				type: Sq.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			qid: {
				type: Sq.STRING(64),
				allowNull: true
			},
			name: {
				type: Sq.STRING(128),
				allowNull: true
			},
			description: {
				type: Sq.STRING(256),
				allowNull: true
			},
			versionNumber: {
				type: Sq.STRING(16),
				allowNull: true
			},
			publicationDate: {
				type: Sq.DATE,
				allowNull: true
			},
			systemRequirementsID: {
				type: Sq.INTEGER,
				allowNull: false,
				references: {
					model: 'system_requirements',
					key: 'id'
				}
			},
			isVersionOf_osProduct: {
				type: Sq.INTEGER,
				allowNull: true
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		const query = 'DROP TABLE IF EXISTS os_version CASCADE;';
		return queryInterface.sequelize.query(query);
	}
};
