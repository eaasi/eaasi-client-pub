'use strict';

const Sq = require('sequelize');

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('os_version_compatible_with_configured_machine', {
			createdAt: Sq.DATE,
			updatedAt: Sq.DATE,
			osVersionID: {
				type: Sq.INTEGER,
				allowNull: false,
				references: {
					model: 'os_version',
					key: 'id'
				}
			},
			configuredMachineID: {
				type: Sq.INTEGER,
				allowNull: true,
				references: {
					model: 'configured_machine',
					key: 'id'
				}
			}
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable('os_version_is_compatible_with_configured_machine');
		return queryInterface.dropTable('os_version_compatible_with_configured_machine');
	}
};
