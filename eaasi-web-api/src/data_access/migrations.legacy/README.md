# Legacy Migrations

This directory contains migrations that were written as part of the legacy metadata model.

They are not currently executed against the database, but are preserved for future potential use.
