'use strict';

const Sq = require('sequelize');

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('software_environment', {
			createdAt: Sq.DATE,
			updatedAt: Sq.DATE,
			id: {
				type: Sq.INTEGER,
				primaryKey: true,
				allowNull: false,
				autoIncrement: true
			},
			name: {
				type: Sq.STRING(128),
				allowNull: false
			},
			description: {
				type: Sq.STRING(512),
				allowNull: true
			},
			helpText: {
				type: Sq.TEXT,
				allowNull: true
			},
			derivedFrom_softwareEnvironmentID: {
				type: Sq.INTEGER,
				allowNull: true,
				references: {
					model: 'software_environment',
					key: 'id'
				}
			},
			hasPart_configuredOsID: {
				type: Sq.INTEGER,
				allowNull: true,
				references: {
					model: 'configured_os',
					key: 'id'
				}
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('software_environment');
	}
};
